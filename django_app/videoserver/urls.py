from . import views
from django.urls import path

app_name = 'videoserver'

urlpatterns = [
    path('', views.showvideo, name='videoupload'),
]
