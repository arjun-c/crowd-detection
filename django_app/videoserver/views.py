from django.shortcuts import render
from .models import Video
from .forms import VideoForm
# from person-tracking import yolov3_deepsort.py

def showvideo(request):
    lastvideo = Video.objects.last()
    videfile = ''
    if lastvideo:
        videofile = lastvideo.videofile
    form = VideoForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        form.save()

    context = {'videofile': videofile,
               'form': form
               }

    return render(request, 'videoserver/videoupload.html', context)


def process_video(request):
    pass